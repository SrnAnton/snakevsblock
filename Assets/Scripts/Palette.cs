﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class Palette : MonoBehaviour
{
    [SerializeField] private Color[] _palette;
    private SpriteRenderer _spriteRenderer;

    private void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();

        if (_palette.Length > 0)
            _spriteRenderer.color = _palette[Random.Range(0, _palette.Length)];
    }
}
