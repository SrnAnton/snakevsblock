﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraResolution : MonoBehaviour
{
    [Header("Developing Screen Resoulution")]
    [SerializeField] int _width;
    [SerializeField] int _height;

    private float _defaultWidth;
    private Camera _camera;

    private void Start()
    {
        _camera = Camera.main;
        _defaultWidth = _camera.orthographicSize * ((float)_width / (float)_height);
    }

    private void Update()
    {
        _camera.orthographicSize = _defaultWidth / _camera.aspect;
    }
}
