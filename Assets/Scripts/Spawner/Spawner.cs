﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : MonoBehaviour
{
    [Header("General")]
    [SerializeField] private Transform _container;
    [SerializeField] private int _repeatCount;
    [SerializeField] private float _distanceBetweenFullLine;
    [SerializeField] private float _distanceBetweenRandomLine;
    [Header("Finish")]
    [SerializeField] private Finish _finishTemplate;
    [Header("Block")]
    [SerializeField] private Block _blockTemplate;
    [SerializeField] private int _blockSpawnChance;
    [Header("Wall")]
    [SerializeField] private Wall _wallTemplate;
    [SerializeField] private int _wallSpawnChance;
    [Header("Bonus")]
    [SerializeField] private Bonus _bonusTemplate;
    [SerializeField] private int _bonusSpawnChance;

    

    private BlockSpawnPoint[] _blockSpawnPoints;
    private WallSpawnPoint[] _wallSpawnPoints;
    private BonusSpawnPoint[] _bonusSpawnPoints;

    private void Start()
    {
        _blockSpawnPoints = GetComponentsInChildren<BlockSpawnPoint>();
        _wallSpawnPoints = GetComponentsInChildren<WallSpawnPoint>();
        _bonusSpawnPoints = GetComponentsInChildren<BonusSpawnPoint>();

        float levelDistance = (_distanceBetweenFullLine + _distanceBetweenRandomLine) * (_repeatCount + 2);
        GenerateElement(new Vector3(_finishTemplate.transform.position.x, 0.0f, _finishTemplate.transform.position.z), _finishTemplate.gameObject);
        GenerateElement(new Vector3(2.8f, levelDistance / 2.0f, 0.0f), _wallTemplate.gameObject, new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, levelDistance, 0.0f));
        GenerateElement(new Vector3(-2.8f, levelDistance / 2.0f, 0.0f), _wallTemplate.gameObject, new Vector3(0.0f, 0.0f, 0.0f), new Vector3(0.0f, levelDistance, 0.0f));

        for (int i = 0; i < _repeatCount; i++)
        {
            MoveSpawner(_distanceBetweenFullLine / 2.0f);
            GenerateRandomElements(_bonusSpawnPoints, _bonusTemplate.gameObject, _bonusSpawnChance);
            GenerateRandomElements(_wallSpawnPoints, _wallTemplate.gameObject, _wallSpawnChance);
            MoveSpawner(_distanceBetweenFullLine / 2.0f);
            GenerateRandomElements(_blockSpawnPoints, _blockTemplate.gameObject);
            GenerateRandomElements(_wallSpawnPoints, _wallTemplate.gameObject, _wallSpawnChance, new Vector3(0.0f, -_wallTemplate.transform.localScale.y, 0.0f));

            MoveSpawner(_distanceBetweenRandomLine / 2.0f);
            GenerateRandomElements(_bonusSpawnPoints, _bonusTemplate.gameObject, _bonusSpawnChance);
            GenerateRandomElements(_wallSpawnPoints, _wallTemplate.gameObject, _wallSpawnChance);
            MoveSpawner(_distanceBetweenRandomLine / 2.0f);
            GenerateRandomElements(_blockSpawnPoints, _blockTemplate.gameObject, _blockSpawnChance);
            GenerateRandomElements(_wallSpawnPoints, _wallTemplate.gameObject, _wallSpawnChance, new Vector3(0.0f, -_wallTemplate.transform.localScale.y, 0.0f));
        }

        GenerateElement(new Vector3(_finishTemplate.transform.position.x, levelDistance, _finishTemplate.transform.position.z), _finishTemplate.gameObject);

        //transform.position = new Vector3(transform.position.x, 0.0f, transform.position.z);
    }

    private void GenerateRandomElements(SpawnPoint[] spawnPoints, GameObject generatedElement, int spawnChance = 100, Vector3 positionOffset = new Vector3(), Vector3 scaleOffset = new Vector3())
    {
        for (int i = 0; i < spawnPoints.Length; i++)
        {
            if(Random.Range(0, 100) <= spawnChance)
            {
                GenerateElement(spawnPoints[i].transform.position, generatedElement, positionOffset, scaleOffset);
            }
        }
    }

    private GameObject GenerateElement(Vector3 spawnpoint, GameObject generatedElement, Vector3 positionOffset = new Vector3(), Vector3 scaleOffset = new Vector3())
    {
        GameObject element = Instantiate(generatedElement, spawnpoint, Quaternion.identity, _container);
        element.transform.localPosition += positionOffset;
        element.transform.localScale += scaleOffset;
        return element;
    }


    private void MoveSpawnerToStart(float distanceY)
    {
        transform.position = new Vector3(0.0f, 0.0f, 0.0f);
    }

    private void MoveSpawner(float distanceY)
    {
        transform.position = new Vector3(transform.position.x, transform.position.y + distanceY, transform.position.z);
    }
}
